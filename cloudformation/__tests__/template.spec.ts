import { json as cicdJson } from '../cicd/template'
import { CloudFormation } from "aws-sdk"

describe('cicd stack', () => {
  it('could be converted to json', () => {
    const obj = JSON.parse(cicdJson);
    expect(obj.Description).toBe('My template')
  });
  it('validate cicd template', () => {
    const cfn = new CloudFormation({apiVersion: '2010-05-15', region: 'ap-northeast-1'})
    cfn.validateTemplate({TemplateBody: cicdJson}, (err, _) => {
      if (err) console.log(err, err.stack);
    });
  });
});
