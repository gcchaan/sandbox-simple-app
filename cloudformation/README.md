### initialize cicd stack

```shell
# set up
$ npm install
```

```shell
# less cloudformation cicd stack template
$ npx ts-node cicd/run.ts | jq . -C | less
```

```shell
# apply cicd stack template
$ npx ts-node cicd/run.ts --apply
```

```shell
# set remote
[FIXME]
$ git remote add ci $(aws cloudformation describe-stacks --stack-name monjament-cicd --query 'Stacks[].Outputs[?OutputKey==`repository`][]|[0].OutputValue')
$ git push ci master
```
