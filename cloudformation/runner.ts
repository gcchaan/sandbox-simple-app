import { execSync } from "child_process"

import { CloudFormation } from "aws-sdk"
import moment from "moment"

export class Stack {
  region: string
  name: string
  baseURI: string
  json: string
  constructor(region, name, json) {
    this.region = region;
    this.name = name;
    this.baseURI = `https://${this.region}.console.aws.amazon.com/cloudformation/home?region=${this.region}`;
    this.json = json;
  }
  stackDetailUrl(stackId) {
    return `${this.baseURI}#/stack/detail?stackId=${stackId}`;
  }
  changeSetUrl(id) {
    return `${this.baseURI}#/changeset/detail?changeSetId=${id}`;
  }
}

export class Runner {
  stack: Stack
  cfn: CloudFormation
  constructor(stack: Stack) {
    this.stack = stack;
    this.cfn = new CloudFormation({apiVersion: '2010-05-15', region: stack.region})
  }
  createStack(stackInput) {
    const createStack = this.cfn.createStack(stackInput).promise();
    createStack.then(
      (data) => {
        console.log('open ' + this.stack.stackDetailUrl(data.StackId));
      });
  }
  createChangeSet(createChangeSetInput) {
    const createChangeSet = this.cfn.createChangeSet(createChangeSetInput).promise();
    createChangeSet.then(
      (data) => {
        console.log('open ' + this.stack.changeSetUrl(data.Id));
    });
  }
  apply(data) {
    const timestamp = moment().format('YYYYMMDDhhmmss');
    const hash = execSync('git rev-parse --short HEAD', {encoding: 'utf8'}).replace(/\n$/, '');//trimが使えない
    const stackInput: CloudFormation.Types.CreateStackInput = {
      StackName: this.stack.name,
      TemplateBody: this.stack.json,
      NotificationARNs: [],// TODO
      Capabilities: ['CAPABILITY_IAM', 'CAPABILITY_NAMED_IAM']
    };
    if (data.StackSummaries.map(_ => _.StackName).indexOf(this.stack.name) > -1) {
      const createChangeSetInput = <CloudFormation.Types.CreateChangeSetInput>stackInput
      createChangeSetInput.ChangeSetName = `${this.stack.name}-${hash}-${timestamp}`
      this.createChangeSet(createChangeSetInput);
    } else {
      this.createStack(stackInput);
    }
  }
  run() {
    const existingStatus = ['CREATE_COMPLETE', 'ROLLBACK_FAILED', 'ROLLBACK_COMPLETE', 'DELETE_FAILED', 'UPDATE_COMPLETE', 'UPDATE_ROLLBACK_FAILED', 'UPDATE_ROLLBACK_COMPLETE'];
    const listStacks = this.cfn.listStacks({StackStatusFilter: existingStatus}).promise();
    listStacks.then(
      (data) => { this.apply(data); }
    ).catch(
      (error) => {
        console.log(error, error.stack);
    });
  }
}
