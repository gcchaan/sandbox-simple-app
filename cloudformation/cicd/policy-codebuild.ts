export function codeBuildPolicy(codeBuild: string, arn: string, bucket: string) { return {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Resource": [
                `arn:aws:logs:ap-northeast-1:383466607609:log-group:/aws/codebuild/${codeBuild}`,
                `arn:aws:logs:ap-northeast-1:383466607609:log-group:/aws/codebuild/${codeBuild}:*`
            ],
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ]
        },
        {
            "Effect": "Allow",
            "Resource": [
                "arn:aws:s3:::codepipeline-ap-northeast-1-*"
            ],
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:GetObjectVersion"
            ]
        },
        {
            "Effect": "Allow",
            "Resource": [
                arn
            ],
            "Action": [
                "codecommit:GitPull"
            ]
        },
        {
            "Effect": "Allow",
            "Resource": [
                `arn:aws:s3:::${bucket}/*`
            ],
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:GetObjectVersion"
            ]
        }
    ]
}}

// console.log(JSON.stringify(codeBuildBasePolicy('name', 'arn')));
