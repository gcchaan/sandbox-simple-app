import {
  Fn,
  S3,
  IAM,
  CodeCommit,
  CodeBuild,
  CodePipeline } from 'cloudform'
import { codeBuildPolicy } from './policy-codebuild'
import { codePipelinePolicy } from './policy-codepipeline'

const artifactBucketName = 'monjament-artifact'
const repositoryName = 'monjament';
const codeBuildName = 'monjament';

export const cicdResources = {
  // Artifact Store
  S3: new S3.Bucket({
    BucketName: artifactBucketName
  }),
  // CodeCommit
  CodeCommit: new CodeCommit.Repository({
    RepositoryName: repositoryName,
    RepositoryDescription: 'monjament repo'
  }),
  // CodeBuild
  CodeBuildServiceRole: new IAM.Role({
    RoleName: 'codebuild-monjament-service-role',
    AssumeRolePolicyDocument: {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Principal": {
            "Service": "codebuild.amazonaws.com"
          },
          "Action": "sts:AssumeRole"
        }
      ]
    },
    Path: '/service-role/',
    Policies: [
      {
        PolicyName: 'codeBuildPolicy-monjament-ap-northeast-1',
        PolicyDocument: codeBuildPolicy(
          codeBuildName,
          `arn:aws:codecommit:ap-northeast-1:383466607609:${repositoryName}`,
          artifactBucketName)
      }
    ]
  }),
  CodeBuild: new CodeBuild.Project({
    Name: codeBuildName,
    Artifacts: {
      // Path?: Value<string> -> アーティファクトを保存するフォルダへのパス
      Type: 'CODEPIPELINE'
    },
    ServiceRole: Fn.GetAtt('CodeBuildServiceRole', 'Arn'),
    Environment: {
      Type: 'LINUX_CONTAINER',
      EnvironmentVariables: [
        /*{// optional. 環境変数 TODO 関数化
          Type: 'PARAMETER_STORE',// optional. PARAMETER_STORE
          Name: 'LOGIN_PASSWORD',
        Value: '/CodeBuild/dockerLoginPassword'}*/
      ],
      PrivilegedMode: false,// Dockerイメージの構築用
      Image: 'node:9.8.0',
      ComputeType: 'BUILD_GENERAL1_SMALL'
    },
    EncryptionKey: 'arn:aws:kms:ap-northeast-1:383466607609:alias/aws/s3',
    Source: {
      Type: 'CODEPIPELINE',
      BuildSpec: 'buildspec.yml',
      GitCloneDepth: 1,
    },
    TimeoutInMinutes: 10
    // Cache?: ProjectCache
  }),
  // CodePipeline
  CodePipelineRole: new IAM.Role({
    AssumeRolePolicyDocument: {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Sid": "",
          "Effect": "Allow",
          "Principal": {
            "Service": "codepipeline.amazonaws.com"
          },
          "Action": "sts:AssumeRole"
        }
      ]
    },
    Path: '/',
    RoleName: 'AWS-CodePipeline-Service-Of-Monjament'
  }),
  codePipelinePolicy: new IAM.Policy({
    Roles: [Fn.Ref('CodePipelineRole')],
    PolicyDocument: codePipelinePolicy(),
    PolicyName: 'codePipelinePolicy-monjament-ap-northeast-1'
  }),
  CodePipeline: new CodePipeline.Pipeline({
    ArtifactStore: {
      Location: Fn.Ref('S3'),
      Type: 'S3'
    },
    RestartExecutionOnUpdate: true,
    RoleArn: Fn.GetAtt('CodePipelineRole', 'Arn'),
    Stages: [{
      Name: 'Source',
      // https://docs.aws.amazon.com/codepipeline/latest/userguide/reference-pipeline-structure.html
      Actions: [{
        Name: 'SourceAction',
        ActionTypeId: {
          Category: 'Source',
          Owner: 'AWS',
          Provider: 'CodeCommit',
          Version: '1'
        },
        Configuration: {
          RepositoryName: repositoryName,
          BranchName: 'master'
        },
        InputArtifacts: [],
        OutputArtifacts: [{
          Name: 'SourceOutput'
        }],
        RunOrder: 1}]
    }, {
        Name: 'Build',
        Actions: [{
          Name: 'BuildAction',
          ActionTypeId: {
            Category: 'Build',
            Owner: 'AWS',
            Provider: 'CodeBuild',
            Version: '1'
          },
          Configuration: {
            ProjectName: codeBuildName
          },
          InputArtifacts: [{
            Name: 'SourceOutput'
          }],
          // 自然数。小さいもの順。同じ数字は並列
          RunOrder: 1}]
    }]
  })
}
