import cloudform, { Fn, S3 } from 'cloudform'
import { Runner, Stack } from '../runner'
import { json } from './template'

const stack = new Stack(
  'ap-northeast-1',
  'monjament-cicd',
  json);
if(process.argv[2] === '--apply') {
  const runner = new Runner(stack)
  runner.run()
} else {
  console.log(json)
}
