import cloudform, { Fn, S3 } from 'cloudform'
import { cicdResources } from './resources'

export const json = cloudform({
  Description: 'My template',
  Parameters: {},
  Mappings: {},
  Resources: cicdResources,
  Outputs: {
    repository: {
      Value: Fn.GetAtt('CodeCommit', 'CloneUrlHttp')
    }
  }
});
