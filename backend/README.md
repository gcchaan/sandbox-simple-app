### Lambda Function

- f-cognito.signUp
  - サインアップ時にドメイン制限する


## AWS Resources

- `template/resources.ts` で追加リソースを管理している


### Operation

テンプレートを確認
```shell
$ npm run build
$ cat .cloudformation/cloudformation-template-update-stack.json | jq . -C | less 
```

Lambda Function のビルドを確認
```shell
$ npx sls webpack
$ less .built/...
```

### Release

```shell
$ npm run test
$ npm run deploy
```
