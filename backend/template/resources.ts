import { content } from './content'
import { data } from './data'
import { user } from './user'

export default (<any>Object).assign({},
  content,
  data,
  user,
)
