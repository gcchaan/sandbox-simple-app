import { Fn, Cognito, IAM, Refs } from 'cloudform'

const assumeRoleWithWebIdentity = (type: string, actions: string[]) => { return {
  AssumeRolePolicyDocument: {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "Federated": "cognito-identity.amazonaws.com"
        },
        "Action": "sts:AssumeRoleWithWebIdentity",
        "Condition": {
          "StringEquals": {
            "cognito-identity.amazonaws.com:aud": {
              "Ref": "CognitoIdentityPool"
            }
          },
          "ForAnyValue:StringLike": {
            "cognito-identity.amazonaws.com:amr": type
          }
        }
      }
    ]
  },
  Policies: [{
    "PolicyName": "root",
    "PolicyDocument": {
      "Version": "2012-10-17",
      "Statement": [{
        "Effect": "Allow",
        "Action": [
          "mobileanalytics:PutEvents",
          "cognito-sync:*",
          "cognito-identity:*"
        ],
        "Resource": [
          "*"
        ]
      }]
    }
  }],
  RoleName: Fn.Join('_', [Fn.GetAtt('CognitoIdentityPool', 'Name'), type, 'Role'])
}}

export const user = {
  AuthRole: new IAM.Role(assumeRoleWithWebIdentity('authenticated', ['_'])),
  UnauthRole: new IAM.Role(assumeRoleWithWebIdentity('unauthenticated', ['_'])),
  CognitoIdentityPool: new Cognito.IdentityPool({
    // ID プールが認証されていないログインをサポートするかどうかを指定
    AllowUnauthenticatedIdentities: true,
    CognitoIdentityProviders: [{
      ClientId: Fn.Ref('CognitoUserPoolClient'),
      ProviderName: Fn.Join('', ['cognito-idp.', Refs.Region, '.amazonaws.com/', Fn.Ref('CognitoUserPool')]),
      ServerSideTokenCheck: true
    }]
  }),
  CognitoIdentityPoolRoleAttachment: new Cognito.IdentityPoolRoleAttachment({
    IdentityPoolId: Fn.Ref('CognitoIdentityPool'),
    Roles: {
      authenticated: Fn.GetAtt('AuthRole', 'Arn'),
      unauthenticated: Fn.GetAtt('UnauthRole', 'Arn')
    }
  }),
  CognitoUserPool: new Cognito.UserPool({
    UserPoolName: Fn.Ref('AWS::StackName'),
    AdminCreateUserConfig: {
      AllowAdminCreateUserOnly: false
    },
    AliasAttributes: ['email', 'preferred_username'],
    AutoVerifiedAttributes: ['email'],
    EmailVerificationSubject: 'ご登録ありがとうございます',
    EmailVerificationMessage: '認証コードは{####}',
    LambdaConfig: {
      PreSignUp: Fn.GetAtt('CognitoSignUpLambdaFunction', 'Arn')
    },
    Policies: {
      PasswordPolicy: {
        MinimumLength: 6,
        RequireNumbers: true,
        RequireUppercase: false,
        RequireLowercase: false,
        RequireSymbols: false
      }
    },
    Schema: [{
      Name: 'email',
      AttributeDataType: 'String',
      Mutable: false,
      Required: true,
    },
    {
      Name: 'preferred_username',
      AttributeDataType: 'String',
      Mutable: false,
      Required: false,
    }]
  }),
  CognitoUserPoolClient: new Cognito.UserPoolClient({
    ClientName: Fn.Ref('CognitoUserPool'),
    GenerateSecret: false,
    UserPoolId: Fn.Ref('CognitoUserPool'),
    RefreshTokenValidity: 7
  })
}
