import { Fn, Refs, S3, CloudFront } from 'cloudform'

const policy = {
  "Statement": [
      {
          "Sid": "PublicReadForGetBucketObjects",
          "Effect": "Allow",
          "Principal": {
            AWS: Fn.Join(' ', [
              'arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity',
              Fn.Ref('CloudFrontOriginAccessIdentity')
            ])
          },
          "Action": "s3:GetObject",
          "Resource": Fn.Join('/', [
            Fn.GetAtt('StaticContentsS3', 'Arn'),
            '*'
          ])
      }
  ]
}

const originId = 'Origin 1';

export const content = {
  StaticContentsS3: new S3.Bucket({
    BucketName: Refs.StackName,
    WebsiteConfiguration: {
      ErrorDocument: 'index.html',
      IndexDocument: 'index.html',
      // RedirectAllRequestsTo?: RedirectAllRequestsTo
      // RoutingRules?: List<RoutingRule>
    }
  }),
  BucketPolicy: new S3.BucketPolicy({
    Bucket: Fn.Ref('StaticContentsS3'),
    PolicyDocument: policy
  }),
  AssetsDistribution: new CloudFront.Distribution({
    DistributionConfig: {
      // Aliases?: List<Value<string>>
      Origins: [{
        Id: originId,
        // OriginCustomHeaders?: List<OriginCustomHeader>
        DomainName: Fn.GetAtt('StaticContentsS3', 'DomainName'),
        S3OriginConfig: {
          OriginAccessIdentity: Fn.Sub('origin-access-identity/cloudfront/${CloudFrontOriginAccessIdentity}', {_: '_'})
        // OriginPath?: Value<string>
        }
      }],
      Comment: Refs.StackName,
      DefaultCacheBehavior: {
        Compress: true,
        TargetOriginId: originId,
        ViewerProtocolPolicy: 'redirect-to-https',
        ForwardedValues: {
          QueryString: false
        }
      },
      DefaultRootObject: 'index.html',
      Enabled: true,
      //ViewerCertificate?: ViewerCertificate
      PriceClass: 'PriceClass_200',
      HttpVersion: 'http2'
    }
  }),
  CloudFrontOriginAccessIdentity: new CloudFront.CloudFrontOriginAccessIdentity({
    CloudFrontOriginAccessIdentityConfig: {
      Comment: Refs.StackName
    }
  })
}
