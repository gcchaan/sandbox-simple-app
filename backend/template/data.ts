import { DynamoDB } from 'cloudform'
import { settings } from 'common/src/index'

export const data = {
  SiteTable: new DynamoDB.Table({
    TableName: settings.db.table,
    AttributeDefinitions: [
      {
        AttributeName: 'owner',
        AttributeType: 'S'
      },
      {
        AttributeName: 'createdAt',
        AttributeType: 'S'
      }
    ],
    KeySchema: [
      {
        AttributeName: 'owner',
        KeyType: 'HASH'
      },
      {
        AttributeName: 'createdAt',
        KeyType: 'RANGE'
      }
    ],
    ProvisionedThroughput: {
      ReadCapacityUnits: 1,
      WriteCapacityUnits: 1
    }
  }),
};
