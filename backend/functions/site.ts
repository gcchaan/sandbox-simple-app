import {
  APIGatewayProxyEvent,
  APIGatewayEventRequestContext,
  APIGatewayProxyCallback
} from 'aws-lambda'
import ow from 'ow'
const moment = require('moment-timezone');
import { DynamoDB } from 'aws-sdk'
import { settings } from 'common'

interface PathParam {
  owner: string;
  createdAt?: string;
}

function validateParam(data: any, callback: APIGatewayProxyCallback) {
  const unicorn = (input: string) => {
    ow(input, ow.string.minLength(1))
  }
  try {
    unicorn(data.owner);
    unicorn(data.title);
    unicorn(data.description);
  } catch(err) {
    console.error(`[Error]: ${err}`);
    console.log(`[LOG]: ${JSON.stringify(data)}`);
    callback(null, {
      statusCode: 400,
      body: `Bad Request: ${err}`,
      headers: { 'Content-Type': 'text/plain' }
    });
  }
}

const okJson = (body: Record<string, any>) => {
  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json; charset=utf-8',
    },
    body: JSON.stringify({ body })
  }
}

const localConfig = process.env.NODE_ENV === 'development' ? {
    endpoint: `http://localhost:${settings.db.localPort}`,
} : {};
const ddc = new DynamoDB.DocumentClient(
  Object.assign(localConfig, {    region: 'ap-northeast-1'}));


export async function list(
    event: APIGatewayProxyEvent,
    context: APIGatewayEventRequestContext,
    callback: APIGatewayProxyCallback): Promise<void> {
  try {
    const scanItems = await ddc.scan({
      TableName: 'SiteTable'
    }).promise();
    const response = okJson({'items': scanItems.Items})

    callback(null, response);
  } catch(err) {
    console.error(`[Error]: ${JSON.stringify(err)}`);
    callback(err);
  }
}

export async function getByOwner(
  event: APIGatewayProxyEvent,
  context: APIGatewayEventRequestContext,
  callback: APIGatewayProxyCallback): Promise<void> {
  try {
    const pathParam: PathParam = <any>event.pathParameters;
    const params = {
      TableName: 'SiteTable',
      KeyConditionExpression: '#hash= :key1 AND #range > :key2',
      ExpressionAttributeNames: {
        '#hash': 'owner',
        '#range': 'createdAt'
      },
      ExpressionAttributeValues: {
        ':key1': pathParam.owner,
        ':key2': '0'
      }
    };
    const queryItems = await ddc.query(params).promise();
    const response = okJson({'items': queryItems.Items});
    callback(null, response);
  } catch(err) {
    console.error(`[Error]: ${JSON.stringify(err)}`);
    callback(err);
  }
}

export async function get(
    event: APIGatewayProxyEvent,
    context: APIGatewayEventRequestContext,
    callback: APIGatewayProxyCallback): Promise<void> {
  try {
    const pathParam: PathParam = <any>event.pathParameters;
    const params = {
      TableName: 'SiteTable',
      Key: {
        owner: pathParam.owner,
        createdAt: pathParam.createdAt
      }
    };
    const getItem = await ddc.get(params).promise();
    const response = okJson({'item': getItem.Item});
    callback(null, response);
  } catch(err) {
    console.error(`[Error]: ${JSON.stringify(err)}`);
    callback(err);
  }
}

export async function create(
    event: APIGatewayProxyEvent,
    context: APIGatewayEventRequestContext,
    callback: APIGatewayProxyCallback): Promise<void> {
  const data = event.body !== null ? JSON.parse(event.body) : '';
  validateParam(data, callback);
  const autoParams = {
    branch: 'test-branch',
    createdAt: moment().tz('Asia/Tokyo').format('YYYYMMDDhhmmss')
  };
  try {
    await ddc.put({
      TableName: 'SiteTable',
      Item: Object.assign(data, autoParams)
    }).promise();
  } catch(err) {
    console.error(`[Error]: ${JSON.stringify(err)}`);
    console.debug(`[Debug]: ${event.body}`);
    callback(null, { statusCode: 500, body: 'Internal Server Error', headers: { 'Content-Type': 'text/plain' } });
  }
  const response = okJson({'item': Object.assign(data, autoParams)});
  callback(null, response);
}
