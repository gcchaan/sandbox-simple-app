export function event(triggerSource = '', userAttributes = null) {
  return {
    triggerSource: triggerSource,
    request: {
      userAttributes: userAttributes,
      validationData: null
    },
    response: {
      autoConfirmUser: false,
      autoVerifyEmail: false,
      autoVerifyPhone: false
    }
  }
}

export const context = {
  callbackWaitsForEmptyEventLoop: false,// should nullable
  done: jest.fn(),
  succeed: () => {},
  fail: () => {},
  logGroupName: '/aws/lambda/validation',
  logStreamName: 'yyyy/mm/dd/[$LATEST]hash',
  functionName: 'validation',
  memoryLimitInMB: 128,
  functionVersion: '$LATEST',
  getRemainingTimeInMillis: () => 1,
  awsRequestId: '',
  invokedFunctionArn: 'arn:aws:lambda:ap-northeast-1:xxxx:function:validation'
}
