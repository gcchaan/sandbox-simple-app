import { event, context } from './data'
import { signUp } from '../validation'

const event1 = event('PreSignUp_SignUp', { email: 'aaa@hoge.ne.jp' });
const event2 = event('PreSignUp_SignUp',{ email: 'aaa@should.rejected.com' });

describe('mytest', () => {
  it('登録できる', () => {
    signUp(event1, context, () => {});
    expect(context.done).toHaveBeenCalledWith(undefined, event1);
  });
  it('ドメインバリデーション', () => {
    signUp(event2, context, () => {});
    expect(context.done).toHaveBeenCalledWith(Error('domain validation error'), event2);
  });
});
