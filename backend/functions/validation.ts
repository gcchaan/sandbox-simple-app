import { CognitoUserPoolTriggerEvent, Handler, Context, Callback } from 'aws-lambda';

const allowedDomains = ['hoge.ne.jp']

export const signUp: Handler = (
        event: CognitoUserPoolTriggerEvent,
        context: Context,
        callback: Callback) => {
  // PreSignUp_SignUp
  if (event.triggerSource == 'PreSignUp_SignUp') {
    if (event.request.userAttributes &&
        event.request.userAttributes.email &&
        allowedDomains.some((_) =>
          event.request.userAttributes.email.endsWith('@' + _))) {
      context.done(undefined, event);
    } else {
      const error = new Error('domain validation error');
      context.done(error, event);
    }
  }
};
