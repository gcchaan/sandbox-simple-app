export const settings = {
  db: {
    table: 'SiteTable',
    localPort: '8000'
  },
  api: {
    localPort: '4000'
  },
  client: {
    localPort: '3000'
  },
  cloudformation: {
    stackBaseName: 'alaska'
  }
};
