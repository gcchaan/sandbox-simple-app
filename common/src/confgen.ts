import { writeFile } from 'fs'
import { Command, flags } from '@oclif/command'
import { CloudFormation } from 'aws-sdk'
import { settings } from './settings'

class Confgen extends Command {

  static description = 'generate configration file'

  static flags = {
    // add --version flag to show CLI version
    version: flags.version({char: 'v'}),
    help: flags.help({char: 'h'}),
    module: flags.string({char: 'm', description: 'module'}),
    stage: flags.string({char: 's', description: 'mode'}),

  }

  async run() {
    const { flags } = this.parse(Confgen)
    const module = flags.module
    const stage = flags.stage || 'development'
    let filename = ''
    let conf
    if (module === 'client') {
      filename = `${stage}.env`
      const cloudformation = new CloudFormation({apiVersion: '2010-05-15', region: 'ap-northeast-1'});
      // FIX LATER
      const res = cloudformation.describeStacks({StackName: `${settings.cloudformation.stackBaseName}-production`}).promise();
      res.then((data) => {
        const serviceEndpoint = data.Stacks![0].Outputs!.find(o => o.OutputKey === 'ServiceEndpoint');
        const apiEndpoint = stage == 'development'
          ? `http://localhost:${settings.api.localPort}`
          : serviceEndpoint!.OutputValue;
        const identityPoolId = data.Stacks![0].Outputs!.find(o => o.OutputKey === 'IdentityPoolId')!.OutputValue;
        const userPoolId = data.Stacks![0].Outputs!.find(o => o.OutputKey === 'UserPoolId')!.OutputValue;
        const userPoolWebClientId = data.Stacks![0].Outputs!.find(o => o.OutputKey === 'UserPoolWebClientId')!.OutputValue;
        const kv = [
          {
            key: 'API_ENDPOINT',
            value: apiEndpoint
          },
          {
            key: 'IDENTITY_POOL_ID',
            value: identityPoolId
          },
          {
            key: 'USER_POOL_ID',
            value: userPoolId
          },
          {
            key: 'USER_POOL_WEB_CLIENT_ID',
            value: userPoolWebClientId
          },
        ]
        conf = kv.map(_ => `${_.key}=${_.value}`).join('\n')
        writeFile(filename, conf + '\n', (err) => {
          if (err) throw err;
        })
      }).catch((error: any) => {
        console.error(error);
      });
    } else if(module == 'backend') {
      filename = `serverless.conf.json`
      conf = JSON.stringify(settings)
      writeFile(filename, conf, (err) => {
        if (err) throw err;
      })
    }
  }
}

export = Confgen
