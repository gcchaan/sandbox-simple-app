const path = require('path');

module.exports = {
  entry: './src/index.ts',

  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, 'public', 'dist')
  },
  resolve: {
    mainFields: ['main', 'module'],
    extensions: ['mjs', '.ts', '.tsx', '.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            'scss': 'vue-style-loader!css-loader!sass-loader',
            'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax',
          }
        },
      },
      {
        test: /\.ts(x)?$/,
        exclude: /node_modules/,
        loader: 'ts-loader',
        options: {
          appendTsSuffixTo: [/\.vue$/],
        }
      },
      // https://github.com/aws/aws-amplify/issues/433
      { enforce: 'pre', test: /\.js$/, exclude: /node_modules/, loader: 'source-map-loader' }
    ]
  },
};
