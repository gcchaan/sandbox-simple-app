const webpack = require('webpack');
const path = require('path');
const merge = require('webpack-merge');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const common = require('./webpack.common.js');

const pathsToClean = [path.join(__dirname, 'public', 'dist', '*')];
module.exports = merge(common, {
  plugins: [
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'production',
      DEBUG: false,
    }),
    new UglifyJSPlugin(),
    new CleanWebpackPlugin(pathsToClean),
    // webpack-merge が promise 対応していないので conf 外で env 設定をする
    new Dotenv({
      path: path.join(__dirname, 'production.env')
    }),
  ],
});
