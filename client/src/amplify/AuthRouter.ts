import * as Components from './components'
import AmplifyStore from './AmplifyStore'
import AuthView from './AuthView'

import { Auth, Logger } from 'aws-amplify'
import { Route } from "vue-router";

const logger = new Logger('AuthRouter');

const AuthRouter = {
  path: '/auth',
  name: 'auth',
  component: AuthView,
  children: [
    {
      path: 'signIn',
      name: 'auth_SignIn',
      component: Components.SignIn
    },
    {
      path: 'signUp',
      name: 'auth_SignUp',
      component: Components.SignUp
    },
    {
      path: 'signOut',
      name: 'auth_SignOut',
      component: Components.SignOut
    },
    {
      path: 'confirmSignUp',
      name: 'auth_ConfirmSignUp',
      component: Components.ConfirmSignUp
    },
    {
      path: 'verifyContact',
      name: 'auth_VerifyContact',
      component: Components.VerifyContact
    },
    {
      path: 'forgotPassword',
      name: 'auth_ForgotPassword',
      component: Components.ForgotPassword
    }
  ]
}

const AuthFilter = (to: Route, from: Route, next: Function) => {
  logger.debug('before routing ', to, from)
  Auth.currentAuthenticatedUser()
    .then(user => {
      logger.debug('...has user', user)
      AmplifyStore.commit('setUser', user)
      Auth.currentCredentials()
        .then(credentials => {
          AmplifyStore.commit('setUserId', credentials.identityId)
        })
        .catch(err => logger.debug('get current credentials err', err))
      next()
    })
    .catch(err => {
      logger.debug('...no user', err)
      AmplifyStore.commit('setUser', null)
      if (to.meta.requiresAuth) {
        next('/auth/signIn')
      } else {
        next()
      }
    })
}

export default AuthRouter
export { AuthFilter }