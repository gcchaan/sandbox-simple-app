import Vue from 'vue'

import SignIn from './SignIn.vue'
import SignUp from './SignUp.vue'
import SignOut from './SignOut.vue'
import ConfirmSignUp from './ConfirmSignUp.vue'
import VerifyContact from './VerifyContact.vue'
import ForgotPassword from './ForgotPassword.vue'
import Greetings from './Greetings.vue'

Vue.component('a-sign-in', SignIn)
Vue.component('a-sign-up', SignUp)
Vue.component('a-sign-out', SignOut)
Vue.component('a-confirm-sign-up', ConfirmSignUp)
Vue.component('a-verify-contact', VerifyContact)
Vue.component('a-forgot-password', ForgotPassword)
Vue.component('a-greetings', Greetings)

export {
  SignIn,
  SignUp,
  SignOut,
  ConfirmSignUp,
  VerifyContact,
  ForgotPassword,
  Greetings
}
