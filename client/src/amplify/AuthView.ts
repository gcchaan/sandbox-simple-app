//import AmplifyTheme from './AmplifyTheme'

const AuthView = {
  template: `
    <div>
      <router-view></router-view>
    </div>
  `,
  data: () => {
    return {
      //theme: AmplifyTheme
    }
  }
}

export default AuthView
