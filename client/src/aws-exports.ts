export const aws_exports = {
  Auth: {
  // REQUIRED - Amazon Cognito Identity Pool ID
    identityPoolId: process.env.IDENTITY_POOL_ID,
  // REQUIRED - Amazon Cognito Region
    region: 'ap-northeast-1',
  // OPTIONAL - Amazon Cognito User Pool ID
    userPoolId: process.env.USER_POOL_ID,
  // OPTIONAL - Amazon Cognito Web Client ID
    userPoolWebClientId: process.env.USER_POOL_WEB_CLIENT_ID,
  },
  API: {
    endpoints: [
      {
        name: 'service',
        endpoint: process.env.API_ENDPOINT
      }
    ]
  }
};
