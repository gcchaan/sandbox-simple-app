import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from './components/Index.vue'
import Detail from './components/Detail.vue'
import User from './components/User.vue'
import  Amplify from 'aws-amplify'
import { aws_exports } from './aws-exports'
import { AuthFilter, AuthRouter } from "./amplify";

const routes = [
  { path: '/', name: 'Index', component: Index },
  { path: '/detail/:id', name: 'Detail', component: Detail, meta: { requiresAuth: true }},
  { path: '/user/:user', name: 'User', component: User },
  AuthRouter
];

const router = new VueRouter({
  routes: routes,
})
router.beforeEach(AuthFilter);

Vue.use(VueRouter);
new Vue({
  router,
  components: {
    Index,
    Detail
  }
}).$mount('#app');

Object.assign(window, {LOG_LEVEL: 'DEBUG'});
Amplify.configure(aws_exports);
